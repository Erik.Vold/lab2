package INF101.lab2;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random ;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

	private int max_size = 20;
	private String sort = " ";
	private LocalDate utlop;
	private FridgeItem incool = new FridgeItem(sort, utlop);
	private Collection<FridgeItem> incoollist = new ArrayList<FridgeItem>();
	private Iterator<FridgeItem> iter = incoollist.iterator();

// constructors
	public void Fridge(){    
	}

	public void Fridge(FridgeItem item){	
		this.incool = item;
	}

//methods

 	@Override
	public int nItemsInFridge() {
		return this.incoollist.size();
	}

  	@Override
	public int totalSize() {
		return max_size ;
	}

 	@Override
	public void emptyFridge() {	 
		this.incoollist.clear();
		return;
	}

 	@Override
	public boolean placeIn(FridgeItem item) {
		if (this.incoollist.size() < max_size){
			return this.incoollist.add(item);
		 }
		return false;
	}

 	@Override
	public void takeOut(FridgeItem item) {
		this.incool = item ; 
		Boolean lykkes = this.incoollist.remove(item);
		if (!lykkes){
			throw new NoSuchElementException ();
		}
		return;
	} 

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> outFromCoollist = new ArrayList<>();
		Collection<FridgeItem> keepOnCoollist = new ArrayList<FridgeItem>();
		int teller = 0;
		for (FridgeItem item : this.incoollist ){
		   	 if (item.hasExpired()) {
				outFromCoollist.add(teller, item);
				teller++;
  		     		}
		}
		for (int i = 0; i < teller; i++) {
			this.incool = outFromCoollist.get(i);
			this.incoollist.remove(this.incool);
		}
		return outFromCoollist ;
	}
}
